import setuptools

setuptools.setup(
    name="test_pipelines",
    version="0.1.5",
    author="julian",
    author_email="julian.ascencio.vasquez@gmail.com",
    description="test package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
